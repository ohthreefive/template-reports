# Estimated IR activity for Greater Glasgow & Clyde

## Table of Contents

1. [Method](#method)
2. [Sites](#sites)
3. [Available sessions](#available-sessions)
4. [Results](#results)
5. [Analysis](#analysis)

## [Method](#table-of-contents)

* A manual crawl of CRIS, a graphical front-end to the Radiology Information System (RIS) used by radiologists was performed and numbers collated.
* March 2018 was the chosen month assessed.
* Annual figures have been extrapolated based on this month.

## [Sites](#table-of-contents)

* Regular activity occurs in Queen Elizabeth University Hospital (QEUH), Gartnavel General Hospital (GGH), Glasgow Royal Infirmary (GRI) and Royal Alexandra Hospital (RAH.)
* Inverclyde Royal Hospital and Vale of Leven Hospital were not assessed.
* In RAH, peripherally inserted central catheters (PICC lines) are performed by IR and are included.
* In Glasgow hospitals, PICC lines are performed by the nurse-led vascular access service and are not included.

## [Available sessions](#table-of-contents)

* A session is defined as either a morning or afternoon IR list.
* Both IR and fluoroscopy procedures are performed in the same room In RAH so IR sessions there are estimated.
* Sessions per week:
	* QEUH: 20
	* GGH: 2
	* GRI: 4
	* RAH: 2
* This is 28 sessions per week.
* This translates to 1456 per week.

## [Results](#table-of-contents)

### Procedural definitions

* Due to the volume of procedures at QEUH, they are subcategorised thus:
	* PV/Ao - peripheral vascular or aortic procedures under the care of vascular surgeons for stenotic, occlusive or aneurysmal disease
	* V-DA - vascular procedures specific to dialysis access (eg. dialysis line insertion or angiograms of arterio-venous fistulae)
	* NV - non-vascular procedures or vascular procedures not covered by the above definitions

### Location

* In QEUH, IR procedures occur in the IR theatres (1st floor radiology) or in the Hybrid theatres (2nd floor theatre block)

### Miscellaneous definitions

* GGH and GRI do not have on site cover every day and some procedures occur on days where there is no routine list.
* This involves an IR consultant and nursing team being made available to travel to that site.
* IR have a ‘flexible’ consultant (Flexi) to cover such cases.
* Procedures within hours (IO) can be performed under local anaesthetic (LA), planned general anaesthetic (GA) or un-planned general anaesthetic (CEPOD)
* Procedures out of hours (OOH) can be performed under local anaesthetic (LA) or un-planned general anaesthetic (CEPOD)
* QEUH has one regular anaesthetic list per month.
* No other site has routine anaesthetic lists.
* Occasional placenta accreta, percreta or increta cases occur in a planned manner at GRI.

### Total procedures

* QEUH:
	* PV/Ao - 63
	* V-DA - 63
	* NV - 70
* Total - 196 (approx. 2352/yr)
* GGH: 23 (276/yr)
* GRI: 43 (516/yr)
* RAH: 42 (504/yr)
* **Total: 304 (3648/yr)**

### QEUH - location of procedures

* IR: 166 **(1992/yr)**
* Hybrid: 30 **(360/yr)**

### Out of hours cases

* OOH: 17 **(204/yr)**
* OOH-LA: 9 (53%)
* OOH-CEPOD: 8 (47%)
* GRI-OOH: 4
* QEUH-OOH: 13

### Flexible cases

* ie. Cases performed in working hours but at a site with no routine IR list that day.
* No Flexi cases at GGH or RAH
* QEUH has regular lists every day so no Flexi cases
* Flexi-GRI: 6 of 43 (14%)
* **Total: 72 cases/yr not covered by current lists**
* Flexi-CEPOD: 3 of 6 (50%)

### Use of general anaesthetic in IR cases

* Overall: 24 cases **(288/yr)**
* IO-GA: 21 cases (20, QEUH, 1 GRI) 
* IO-CEPOD: 3 cases (2 QEUH, 1 GRI) (13%)

## [Analysis](#table-of-contents)

Based on March 2018:

* 1456 IR sessions/yr in GG&C
* 3646 procedures performed
* 71% of sessions & 64% of cases at QEUH
* 360 cases/yr in hybrid theatres; 18% of QEUH cases
* 204 out of hours IR procedures/yr; 47% needed emergency general anaesthetic
* 72 in hours cases performed/yr when there is no routine IR list; staff transfer required
* As these tend to be emergencies, 50% of them also required general anaesthetic by CEPOD team
* 288 general anaesthetic cases/yr, 13% of which were emergencies requiring general anaesthetic by CEPOD team
