# v 0.02

* changed focus to procedure-based snippets to reduce number of snippets
* individual snippets will be more complicated, however

# v 0.01

* first 4 urology procedures added
* common femoral access variants added
* common consent techniques added
* common medications added
* 'access', 'consent', 'medication' and 'procedures' cheat sheet markdown table
  documents added

# v 0.0

* repository created!
