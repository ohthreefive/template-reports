## Referral pathway to Interventional Radiology (IR) out of hours.

### Situation

- The demands of sustaining the IR on call service are significant and
  specifically there is a subjective increase in unnecessary calls to the on call
  consultant
- This can lead to an impact on work during the day

---

### Background

- The IR out of hours service is currently delivered by 8 consultants filling a
  1 in 9 rota
- There are also two IR nurses
- There is also one radiographer to cover QEUH and one for GRI
- With the exception of the GRI radiographer, who performs a role in the
  diagnostic rota overnight, all members of staff are based at home
- Currently, there are two IR registrars on the on call rota
- Based on 2014 data, around 250 procedures are performed out of hours annually
- Attempts to capture the additional work load (ie. calls to the service not
  resulting in a procedure) have never been accurately captured
- The service receives patients from outside of the health board despite no
  formal pathway existing
- Based on recent data, one in four out of hours procedures is not performed for
  a Greater Glasgow & Clyde patient

---

### Assessment

- The following types of call have been received by members of the IR team:
- The 'Heads up': a clinician has a patient who may need a procedure based on
  the results of imaging which they have just requested
    - The perceived purpose of this call is to have the IR team ready in case an
      intervention is required
    - However, the imaging may show no indication to intervene
    - The imaging may show a non-IR intervention is warranted
    - The patient may stabilise before the imaging is performed and interpreted
    - There may be delay between the clinician's call, the imaging being
      performed, interpreted and fed back to the clinician
    - The radiographer and nurses will not be notified until a procedure is
      planned
    - There is therefore no reason to make this call and no action performed
      upon receiving this call; it merely disrupts the IR consultant
- Asking for permission or guidance in imaging a bleeding patient: a clinician
  has a patient who they believe is bleeding and either want to arrange a CT or
  want to know whether they should arrange a CT
    - Almost without exception, CT angiography will be required
    - The Reporting Centre can justify and report the images, seeking advice in
      equivocal cases if required
- Referrals from other hospitals
    - After a recent case discussed at the IR morbidity and mortality meeting,
      it was agreed that a local clinical team should be involved early in the
      decision-making process to transfer a patient for a procedure

---

### Response

- We propose that calls to IR should be screened by Radiology registrars in the
  Reporting Centre in a way that minimises unnecessary disruptive calls to IR
  while also minimising both the workload on the Reporting Centre and the friction
  for clinicians wishing to refer to IR

- Suggested model:
    - Clincian calls switchboard to refer to IR
    - Switchboard informs clinician they will be connected to Reporting Centre
      for screening questions
    - Reporting Centre radiologist will ask the purpose of the call
    - If the patient has not yet had imaging, this will be arranged by the
      Reporting Centre and the clinician will be advised not to contact IR until the
      scan has been informed and interpreted
    - If the call is from another hospital, the Reporting Centre will ascertain
      whether the patient has had adequate local imaging and whether they have been
      discussed with and accepted by a GG&C clinician
    - If not, they should be advised to do so before referring to IR
    - If these simple screening questions are passed, the caller should be
      passed back to switchboard to be connected to the IR team

---
